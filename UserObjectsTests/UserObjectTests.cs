﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UserObjects.Adapters;
using UserObjects.Command;
using UserObjects.Interfaces;
using UserObjects.Objects;
using UserObjects.SubObjects;
using Xunit;

namespace UserObjectsTests;

public class UserObjectTests:IClassFixture<TestFixture>
{
    private readonly IUserObject _userObject;

    public UserObjectTests(TestFixture testFixture)
    {
        _userObject = testFixture.UserObject;
    }

    [Fact]
    public void UserObject_MoveFormKnownPositionWithKnownVelocity_ReturnCorrectPosition()
    {
        //Assign 
        _userObject.SetProperty("Position", new Vector<int>(new int[8] { 12, 5,0,0,0,0,0,0 }));
        _userObject.SetProperty("Velocity", 1 );
        _userObject.SetProperty("Direction", new Direction(8, 3));
        _userObject.SetProperty("DirectionsNumber", 8);
        Move move = new Move(new MovableObject(_userObject));

        //Act
        move.Execute();

        // Assert
        Assert.StrictEqual( new Vector<int>(new int[8] {11,6,0,0,0,0,0,0}), _userObject.GetProperty("Position"));
    }

    [Fact]
    public void UserObject_TryToMoveObjectWithUndefCurrentPosition_ThrowException()
    {
        //Assign 
        var mock = new Mock<UserObject>();
        mock.SetupGet(a =>a.Position).Throws(new KeyNotFoundException());
        var userObject = mock.Object;
        userObject.SetProperty("Velocity", 1);
        userObject.SetProperty("Direction", new Direction(8, 3));
        userObject.SetProperty("DirectionsNumber", 8);
        Move move = new Move(new MovableObject(mock.Object));

        //Act and Assert
        Assert.Throws<TargetInvocationException>(() => move.Execute());
    }

    [Fact]
    public void UserObject_TryToMoveObjectWithUndefVelocity_ThrowException()
    {
        //Assign 
        var mock = new Mock<UserObject>();
        mock.SetupGet(a => a.Velocity).Throws(new KeyNotFoundException());
        var userObject = mock.Object;
        _userObject.SetProperty("Position", new Vector<int>(new int[8] { 12, 5, 0, 0, 0, 0, 0, 0 }));
        //userObject.SetProperty("Velocity", 1);
        userObject.SetProperty("Direction", new Direction(8, 3));
        userObject.SetProperty("DirectionsNumber", 8);
        Move move = new Move(new MovableObject(mock.Object));

        //Act and Assert
        Assert.Throws<TargetInvocationException>(() => move.Execute());
    }

    [Fact]
    public void UserObject_TryToMoveObjectWithUnposibbleSetPosition_ThrowException()
    {
        //Assign 
        var mock = new Mock<UserObject>();
        mock.Setup(a => a.Position).Throws(new KeyNotFoundException());
        var userObject = mock.Object;
        _userObject.SetProperty("Position", new Vector<int>(new int[8] { 12, 5, 0, 0, 0, 0, 0, 0 }));
        userObject.SetProperty("Velocity", 1);
        userObject.SetProperty("Direction", new Direction(8, 3));
        userObject.SetProperty("DirectionsNumber", 8);
        Move move = new Move(new MovableObject(mock.Object));

        //Act and Assert
        Assert.Throws<TargetInvocationException>(() => move.Execute());
    }

    [Fact]
    public void UserObject_RotateFormKnownDirectionWithKnownAngularVelocity_ReturnCorrectDirection()
    {
        //Assign 
        _userObject.SetProperty("Position", new Vector<int>(new int[8] { 12, 5, 0, 0, 0, 0, 0, 0 }));
        _userObject.SetProperty("AngularVelocity", 1);
        _userObject.SetProperty("Direction", new Direction(8, 3));
        _userObject.SetProperty("DirectionsNumber", 8);
        Rotate rotate = new Rotate(new RotatableObject(_userObject));

        //Act
        rotate.Execute();

        var t1 = (new Direction(8, 4)).GetType();
        var t2= _userObject.GetProperty("Direction").GetType(); 
        // Assert
        Assert.Equal(new Direction(8,4), (Direction)(_userObject.GetProperty("Direction")));
    }
}
