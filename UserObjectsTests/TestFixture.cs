﻿using UserObjects.Interfaces;
using UserObjects.Objects;

namespace UserObjectsTests
{
    public class TestFixture:IDisposable
    {
        public IUserObject UserObject { get; private set; }
        public TestFixture()
        {
            UserObject = new UserObject();
        }
        public void Dispose()
        {
        }
    }
}