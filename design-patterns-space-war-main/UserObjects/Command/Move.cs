﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UserObjects.Interfaces;

namespace UserObjects.Command;

public class Move
{
    private readonly IMovable _movableObject;
    public Move(IMovable movableObject)
    {
        _movableObject = movableObject;
    }

    public void Execute()
    {
        _movableObject.SetPosition(Vector.Add(
            _movableObject.GetPosition(),
            _movableObject.GetVelocity()
            ));
    }

}
