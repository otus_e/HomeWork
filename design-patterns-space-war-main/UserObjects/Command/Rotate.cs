﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UserObjects.Interfaces;

namespace UserObjects.Command;

public class Rotate
{
    private readonly IRotatable _rotatableObject;
    public Rotate(IRotatable rotatableObject)
    {
        _rotatableObject = rotatableObject;
    }

    public void Execute()
    {
        var currentDirection = _rotatableObject.GetDirection();
        var currentAngularVelocity = _rotatableObject.GetAngularVelocity();
        var newDirection = currentDirection.Next(currentAngularVelocity);
        _rotatableObject.SetDirection(newDirection);
    }
}
