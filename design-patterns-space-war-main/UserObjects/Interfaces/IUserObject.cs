﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace UserObjects.Interfaces;

public interface IUserObject
{
    object GetProperty(string name);
    void SetProperty(string name, object value);

}
