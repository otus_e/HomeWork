﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserObjects.Interfaces;

namespace UserObjects.SubObjects;

public class Direction : IDirection, IComparable<Direction>
{
    private readonly int _directionsNumber;
    private int _currentDirection;
    public Direction(int directionNumbers, int currentDirection)
    {
        _directionsNumber = directionNumbers;
        _currentDirection = currentDirection;
    }
    public int DirectionsNumber => _directionsNumber;
    public int CurrentDirection { get => _currentDirection; }

    public int CompareTo(Direction obj)
    {
        return obj._directionsNumber - _directionsNumber + (obj.CurrentDirection - CurrentDirection);
    }

    public IDirection Next(int numberOfSkippedPositions)
    {
        return new Direction(_directionsNumber, (_currentDirection + numberOfSkippedPositions) % _directionsNumber);
    }
}
